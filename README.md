# Writing ECMAScript2015+

## Introduction

> What is ECMAScript?

ECMAScript is the standard definition that is implemented by JavaScript.

> Where is it applicable?

- Frontend (browser)
- Backend (node.js, npm)

## Hello World

```javascript
const name = "World"; // variables: const/let

console.log(`Hello ${name}!`);      // templating strings
console.log("Hello " + name + "!"); // string concatenation
console.log('Hello ' + name + '!'); // more common

console.log(`\
Some multi-
line output\
`);
console.log('Some multi-\nline output');
console.log('Some multi-\n' +
'line output');
```

## Environment

- Frontend: [global `window` object](https://developer.mozilla.org/en-US/docs/Web/API/Window)
- Backend: [node.js API](https://nodejs.org/api/) (e.g. global `process`, `module` objects)
- Full-Stack: [NPM ecosystem](https://www.npmjs.com/)

## Types

```javascript
typeof name === "string"; // true
```

- Simple
    - **undefined**: `undefined,void 0`
    - **boolean**: `true,false`
    - **string**: `` 'some value',"some value",`some value` ``
    - **number**: `-1.3,0,-0,NaN,Infinity,-Infinity`
    - **object**: `null,{x: 2, y: null, z: 'world'},[2,null,'world']`
    - **function**: `(x) => x * x, (x, y) => { return x * y; }, function add(x,y) { return x + y; }, class { /*...*/ }`
- Advanced
    - **symbol**: `Symbol('my symbol'),Symbol.iterator`

Best practice: Declare all variables at the top of their scope.

### Variable defintion

```javascript
// definition
const myConstant = {value: 42};
let myVariable = 'some string';

// assignments
myConstant.value = 21; // see Object.freeze or use libraries for immutability
myVariable = myConstant.value; // type-mixing is bad practice (except for initial null value)

let {value} = myConstant; // value === 21
```

### Comparison

```javascript
// do not use:
1 == '1'

// do use instead:
myObject == null // exception for above: check for null or undefined
myObject === null || myObject === void 0
1 !== '1'
NaN !== NaN
```

### Numbers

```javascript
Number.EPSILON // 2.220446049250313e-16
Number.isNaN(NaN); // true
Number.parseInt('3.1'); // 3
Number.parseFloat('3.1'); // 3.1
Math.abs(Math.sin(2 * Math.PI)) < Number.EPSILON * 2; // true
```

### Objects

```javascript
null
{
  x: 'some value', // key-value mapping
  y: null,
  z,               // same key as variable name
  [key]: value,    // calculated keys
  ...otherObject,  // merge otherObject
}
[
  2,
  null,
  'world',
  ...someArray,
]
```

### Functions

ECMAScript is prototype-based. Thus each function is a class-like structure.

```javascript
let three = new SomeClass(3);

three.plus(4); // 7

SomeClass.prototype.plus.call(new SomeClass(3.5), 4); // 7.5 or 7 depending on implementation below
three.plus.call(null, 4); // 7 or throws Error depending on implementation below

SomeClass.staticNoop({error: null});
```

```javascript
function SomeClass(value) { this.value = value; };

SomeClass.prototype.plus = function (x) { return this.value + x; }
SomeClass.staticNoop = function ({error}) {}
```

```javascript
function SomeClass(value) {
  this.plus = (x) => value + x;
}
SomeClass.staticNoop = ({error}) => {};
```

```javascript
class SomeClass { // caution: while functions are hoisted, class syntax is not
  constructor(value) { this.value = value; }
  plus(x) { return this.value + x; }
  static staticNoop({error}) {}
}
```

- `this` keyword is required for all attribute/method access
- everything is kind-of public; use jsdoc and/or `_` prefix for private attributes/methods
- `Function#apply` and `Function#call` exist for manual context definition

## Language function-alike keywords

`typeof`, `void` and `instanceof` keywords can be used like functions with optional parentesis.

## Comments

```javascript
// single-line comment
/*
 multi-line comment
*/
/**
 * JSDoc
 */
```

## Operators

- bitwise operators: `^` `|` `&` `~`
- unary: `!true` `+'4.3'`
- binary: `&&` `||` (lazy evaluation, return value is result of last evaluation)
    - `true || 5; // true`
    - `true && NaN || 42 || 5; // 42`
- ternary: `true ? 42 : 5; // 42`

## Example

```javascript
class EventBus {

  constructor() {
    this._listener = {};
  }

  on(type, cb) {
    if (typeof type !== 'string' || typeof cb !== 'function') { throw new Error('Invalid parameter.'); }
    if (!this._listener.hasOwnProperty(type)) { this._listener[type] = []; }
    this._listener[type].push(cb);
  }

  emit(type, data) {
    if (typeof type !== 'string') { throw new Error('Invalid parameter.'); }
    if (!this._listener.hasOwnProperty(type)) { return; }
    for (let i = 0; i < this._listener[type].length; i++) {
      this._listener[type][i].call(this, data);
    }
  }

}
```

## Loops

```javascript
let obj = {x: 3, y: 1};
let arr = [4, 1, 5];
for (let i = 0; i < arr.length; i++) { console.log(arr[i]); } // 4 1 5
for (let i in arr) { console.log(i); } // 0 1 2
for (let i of arr) { console.log(i); } // 4 1 5
for (let i in obj) { console.log(i); } // "x" "y"
for (let i of obj) { console.log(i); } // TypeError
```

## Modules

- ECMAScript standardized module system is not (yet) implemented in browsers or node.js
- Transpilation (or on-the-fly compatibility layer) is needed, such as [`babel`](https://babeljs.io/) and [`babel-cli`](https://babeljs.io/docs/usage/cli/)

```javascript
import _ from 'lodash';
import {reduce, merge} from 'lodash';
import {value as aValue} from './a';

export default {value: aValue};
export {aValue};
```

## Promises

```javascript
function fetchData(id, cb) {
  setTimeout(() => { cb(null, {id}); }, 1000);
}

function fetch(id) {
  return new Promise((resolve, reject) => {
    fetchData(id, (err, data) => {
      if (err != null) { return reject(err); }
      resolve(data);
    });
  });
}
```

```javascript
let promise = fetch(42);

promise = promise
  .then(fetchNext)
  .catch(console.error);
```

```javascript
async function fetchNext(id) {
  // return fetch(id + 1);
  return await fetch(id + 1);
}

let promise = fetchNext(42);
```
